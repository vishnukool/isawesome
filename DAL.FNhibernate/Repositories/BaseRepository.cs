﻿using System;
using System.Diagnostics;
using NHibernate;

namespace DAL.FNhibernate.Repositories
{
    public class BaseRepository
    {
        private readonly ISessionFactory sessionFactory;
        public BaseRepository(ISessionFactory sessionFactory)
        {
            this.sessionFactory = sessionFactory;
        }

        protected ISession GetSession()
        {
            ISession session = null;
            try
            {
                session = sessionFactory.GetCurrentSession();
            }
            catch (Exception webReqSessionException)
            {
                try
                {
                    session = sessionFactory.GetCurrentSession();
                }
                catch (Exception threadSessionException)
                {
                    Debug.WriteLine("Cannot obtain session from web or thread static context " + webReqSessionException.StackTrace + "\r\nThread Session Exception: " + threadSessionException.StackTrace);
                }
            }

            return session;
        }

    }
}