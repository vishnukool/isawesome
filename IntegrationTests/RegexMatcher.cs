﻿using System;
using System.Text.RegularExpressions;
using NUnit.Framework;

namespace IntegrationTests
{
    public class RegexMatcher
    {
        [Test]
        public void ShouldWhitelistDomainNametoDotUnderscoreAndHyphenAsSpecialCharacters()
        {
            string subdomain = "Z2ko_ol2-mama.kiAS--ad78..s";
            bool isMatch = Regex.IsMatch(subdomain, @"^[0-9a-zA-Z_\.-]*$");
            Console.WriteLine(isMatch);
        }
    }
}