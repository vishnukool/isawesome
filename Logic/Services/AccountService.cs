﻿using System;
using DAL.FNhibernate.Repositories;

namespace Logic.Services
{
   public class AccountService : IAccountService
   {
       private IUserRepository userRepository;

       public AccountService(IUserRepository userRepository)
       {
           this.userRepository = userRepository;
       }

       public void CreateUserProfile(string userName, string friendlyName)
       {
           userRepository.CreateUser(userName, friendlyName);
       }
    }

    public interface IAccountService
    {
       void CreateUserProfile(string userName, string friendlyName);
    }

    public interface IResponse
    {
    }
}
