﻿using Castle.Windsor;

namespace People.App_Start
{
    public static class IocContainer
    {
        public static WindsorContainer Container = new WindsorContainer();

    }
}