﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using DAL.FNhibernate.Entities;
using DAL.FNhibernate.Repositories;

namespace People.Controllers
{
    public class SearchController : Controller
    {
        private readonly IUrlRepository urlRepository;
        readonly string domainName = ConfigurationManager.AppSettings["DomanName"];
        int UrlsPerPage = int.Parse(ConfigurationManager.AppSettings["SearchResultsPerPage"]);

        public SearchController(IUrlRepository urlRepository)
        {
            this.urlRepository = urlRepository;
        }

        public ActionResult Index(string query, int pageNumber = 0)
        {
//            if (string.IsNullOrEmpty(query) || query.Length < 2)
            if (false)
            {
                return View("Search");
            }
            int count;
            IList<UrlMapping> urlMappings = urlRepository.GetUrls(query, pageNumber, out count);

            ViewBag.DomainNameWithDot = "." + domainName;
            ViewBag.query = query;
            ViewBag.IsPreviousLinkVisible = pageNumber > 0;
            ViewBag.IsNextLinkVisible = UrlsPerPage * (pageNumber+1) < count;
            ViewBag.PageNumber = pageNumber;
            ViewBag.TotalCount = Math.Ceiling((decimal)count/(UrlsPerPage*(pageNumber+1)));
            
            return View(urlMappings);    
        }

    }
}