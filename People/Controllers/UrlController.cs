﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using DAL.FNhibernate.Entities;
using DAL.FNhibernate.Repositories;
using Logic.Services;
using Microsoft.Security.Application;
using Microsoft.Web.Helpers;
using People.Attributes;
using People.Models;
using System.Linq;
using People.Utility;

namespace People.Controllers
{
    public class UrlController : Controller
    {
        private IUrlRepository urlRepository;
        private IPostRepository postRepository;
        private readonly IUserRepository userRepository;
        private readonly ICommentsRepository commentsRepository;
        private readonly IHostHeaderService hostHeaderService;
        readonly string domainName = ConfigurationManager.AppSettings["DomanName"];
        readonly string privateKey = ConfigurationManager.AppSettings["RecaptchaPrivateKey"];
        readonly string websiteName = ConfigurationManager.AppSettings["SiteName"];

        public UrlController(IUrlRepository urlRepository, IPostRepository postRepository, IUserRepository userRepository,
            ICommentsRepository commentsRepository, IHostHeaderService hostHeaderService)
        {
            this.urlRepository = urlRepository;
            this.postRepository = postRepository;
            this.userRepository = userRepository;
            this.commentsRepository = commentsRepository;
            this.hostHeaderService = hostHeaderService;
        }

        public ActionResult Index(string id)
        {
            //todo: parse for special characters here 
            var url = urlRepository.GetUrlWithPost(id);

            if (url != null && url.Posts != null)
            {
                var posts = url.Posts;
                ViewBag.IsAdminOrOwner = posts.UserProfile.UserName == User.Identity.Name;
                ViewBag.Title = id;
                url.Hits++;
                ViewBag.Hits = url.Hits;
                return View(posts);
            }
            ViewBag.DomainName = domainName;
            return View("PageNotFound");
        }

        public ActionResult IframeBody(int id)
        {
            var posts = postRepository.GetPost(id);
            return View("Iframe", (object)posts.Body);
        }

        [Authorize]
        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.DomainNameWithDot = "." + domainName;
            return View(new PostsViewModel());
        }
        
        [ValidateInput(false)]
        [Authorize]
        [HttpPost]
        public ActionResult Create(PostsViewModel postsViewModel)
        {
            ViewBag.DomainNameWithDot = "." + domainName;
            var isCaptchaValid = ReCaptcha.Validate(privateKey);
            if (ModelState.IsValid && isCaptchaValid)
            {
                UserProfile userProfile = userRepository.GetUser(User.Identity.Name);
                string subDomain = postsViewModel.Subdomain;
                var url = urlRepository.GetUrl(subDomain);
                if (url != null)
                {
                    ModelState.AddModelError("subDomainChosen", "Subdomain already taken, please choose another");
                    return View(postsViewModel);
                }
//                    var fullSubdomainWithPostfix = subDomain + "." + websiteName + ".com";
                var posts = new Posts
                    {
                        Body = HtmlUtility.Instance.SanitizeHtml(postsViewModel.Body),
                        Title = HtmlUtility.Instance.SanitizeHtml(postsViewModel.Title),
                        Samay = DateTime.UtcNow,
                        Commentable = postsViewModel.Commentable,
                        ShowHits = postsViewModel.ShowHits,
                        UserProfile = userProfile,
                        Url = subDomain
                    };
//                    hostHeaderService.AddHostHeader(websiteName, fullSubdomainWithPostfix);

                var urlMapping = new UrlMapping
                {
                    Url = subDomain,
                    Posts = posts
                };
                urlRepository.Save(urlMapping);
                return Redirect("http://" + subDomain + "." + domainName);    
            }
            return View(postsViewModel);
        }

        public JsonResult IsDomainFree(string Id)
        {
            if (string.IsNullOrEmpty(Id))
            {
                return null;
            }
            var urlMapping = urlRepository.GetUrl(Id);
            if (urlMapping == null)
            {
                return Json(new { isFree = true });
            }
            return Json(new { isFree = false });
        }

        [MyPostUserAuthorization]
        [HttpPost]
        public JsonResult Delete(int id)
        {
            Posts post = postRepository.GetPost(id);
            UrlMapping urlWithPost = urlRepository.GetUrlWithPost(post);
            
//            string subDomain = urlWithPost.Url;

            urlRepository.Delete(urlWithPost);
            
//            var fullSubdomainWithPostfix = subDomain + "." + websiteName + ".com";
//            hostHeaderService.RemoveHostHeader(websiteName, fullSubdomainWithPostfix);

            return Json(new { success = true });
        }

        [HttpPost]
        public JsonResult DelteComment(int id)
        {
            Comments commentsWithPost = commentsRepository.GetCommentsWithPost(id);
            if (commentsWithPost == null)
            {
                return Json(new
                    {
                        IsSuccess = false,
                        Id = id,
                        Message = "Comment already deleted"
                    });
            }
            if (commentsWithPost.Posts.UserProfile.UserName == User.Identity.Name)
            {
                commentsRepository.Delete(commentsWithPost);
                return Json(new
                {
                    IsSuccess = true,
                    Id = id
                });    
            }
            return Json(new
                {
                    error = "You're not authorised to delete Comments"
                });
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Comments(CommentsModel comments)
        {
            var isCaptchaValid = ReCaptcha.Validate(privateKey);
            if (ModelState.IsValid && isCaptchaValid)
            {
                Posts post = postRepository.GetPost(comments.PostId);
                if (post == null)
                {
                    return RedirectToAction("Index");
                }
                post.Comments.Add(new Comments
                {
                    Body = HtmlUtility.Instance.SanitizeHtml(comments.Body),
                    Email = comments.Email,
                    Name = comments.Name,
                    Samay = DateTime.UtcNow,
                    Posts = post
                });
                postRepository.Save(post);
                return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
            }
            string errors = string.Join(@"<br \>", ModelState.Values
                                                        .SelectMany(x => x.Errors)
                                                        .Select(x => x.ErrorMessage));
            if (!isCaptchaValid)
            {
                errors = string.Join(@"<br \>",
                                 "The characters you entered donot match the one in image. Please try again ..");    
            }
            
            return Json(new
                {
                    errors
                });
        }

        [HttpGet]
        [MyPostUserAuthorization]
        public ActionResult Edit(int id)
        {
            ViewBag.DomainNameWithDot = "." + domainName;
            Posts posts = postRepository.GetPost(id);
            if (posts!=null)
            {
                var postsViewModel = new PostsViewModel
                {
                    Body = posts.Body,
                    Id = posts.Id,
                    Commentable = posts.Commentable,
                    Subdomain = posts.Url,
                    Title = posts.Title
                };
                return View(postsViewModel);    
            }
            return View("AccessDenied");
        }

        [ValidateInput(false)]
        [MyPostUserAuthorization]
        [HttpPost]
        public ActionResult Edit(PostsViewModel postsModel)
        {
            if (ModelState.IsValid)
            {
                Posts post = postRepository.GetPost(postsModel.Id);
                string url = urlRepository.GetUrlForPost(post);
                //todo: plzz do some sql injection and spl characters checks ?? If you don't fix this and go to prod, you will be screwed ! SCREWED !
                post.Body = HtmlUtility.Instance.SanitizeHtml(postsModel.Body);
                post.Title = HtmlUtility.Instance.SanitizeHtml(postsModel.Title);
                post.Commentable = postsModel.Commentable;
                post.ShowHits = postsModel.ShowHits;
                postRepository.Save(post);

                return Redirect("http://" + url + "." + domainName);
            }
            return View(postsModel);
        }
    }
}