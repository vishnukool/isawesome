﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Castle.MicroKernel;

namespace People.Infrastructure
{
    public class WindsorControllerFactory : DefaultControllerFactory
    {
        private readonly IKernel kernel;

        public WindsorControllerFactory(IKernel kernel)
        {
            this.kernel = kernel;
        }

        public override void ReleaseController(IController controller)
        {
            kernel.ReleaseComponent(controller);
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type contollerType)
        {
            if (contollerType == null)
            {
                throw new HttpException(404, string.Format("The following request couldn't be processed \n"
                    + requestContext.HttpContext.Request));
            }
            return (IController)kernel.Resolve(contollerType);
        }
    }
}