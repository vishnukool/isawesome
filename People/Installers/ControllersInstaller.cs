﻿using Castle.Core;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using DAL.FNhibernate;
using DAL.FNhibernate.Repositories;
using Logic.Services;
using NHibernate;
using People.Controllers;

namespace People.Installers
{
    public class ControllersInstaller : IWindsorInstaller
    {
        #region IWindsorInstaller Members

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
//            ISessionFactory sessionFactory = new NHibernateBase().CreateSesionFactory();

            container.Register(
                Component.For<ISessionFactory>().Instance(StaticSessionManager.SessionFactory).LifeStyle.Is(
                    LifestyleType.Singleton),
                Component.For<IHostHeaderService>().ImplementedBy<HostHeaderService>().LifeStyle.Is(
                    LifestyleType.Transient),
                Component.For<IUserRepository>().ImplementedBy<UserRepository>().LifeStyle.Is(
                    LifestyleType.Transient),
                Component.For<IUrlRepository>().ImplementedBy<UrlRepository>().LifeStyle.Is(
                    LifestyleType.Transient),
                Component.For<ICommentsRepository>().ImplementedBy<CommentsRepository>().LifeStyle.Is(
                    LifestyleType.Transient),
                Component.For<IPostRepository>().ImplementedBy<PostsRepository>().LifeStyle.Is(
                    LifestyleType.Transient),
                Component.For<IAccountService>().ImplementedBy<AccountService>().LifeStyle.Is(LifestyleType.PerWebRequest),
                Component.For<HomeController>().ImplementedBy<HomeController>().LifeStyle.Is(
                    LifestyleType.PerWebRequest),
                Component.For<ThrowUpController>().ImplementedBy<ThrowUpController>().LifeStyle.Is(
                    LifestyleType.PerWebRequest),
                Component.For<ErrorController>().ImplementedBy<ErrorController>().LifeStyle.Is(
                    LifestyleType.PerWebRequest),
                Component.For<ProfileController>().ImplementedBy<ProfileController>().LifeStyle.Is(
                    LifestyleType.PerWebRequest),
                Component.For<TermsController>().ImplementedBy<TermsController>().LifeStyle.Is(
                    LifestyleType.PerWebRequest),
                Component.For<PrivacyController>().ImplementedBy<PrivacyController>().LifeStyle.Is(
                    LifestyleType.PerWebRequest),
                Component.For<ContactUsController>().ImplementedBy<ContactUsController>().LifeStyle.Is(
                    LifestyleType.PerWebRequest),
                Component.For<AccountController>().ImplementedBy<AccountController>().LifeStyle.Is(
                    LifestyleType.Transient),
                Component.For<UrlController>().ImplementedBy<UrlController>().LifeStyle.Is(
                    LifestyleType.Transient),
                Component.For<SearchController>().ImplementedBy<SearchController>().LifeStyle.Is(
                    LifestyleType.Transient)
                );
        }



        #endregion
    }
}