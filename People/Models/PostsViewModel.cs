﻿using System.ComponentModel.DataAnnotations;

namespace People.Models
{
    public class PostsViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter a Title for your site")]
        public string Title { get; set; }
        
        [Required(ErrorMessage = "Please enter a sub-url for the site")]
        [RegularExpression(@"^[0-9a-zA-Z_\.-]+$", ErrorMessage = "Allowed characters for subdomain are alphanumerals and the following special characters . - _ ")]
        public string Subdomain { get; set; }

        [Required(ErrorMessage = "Please enter some content for the site")]
        public string Body { get; set; }
        
        public bool Commentable { get; set; }
        public bool ShowHits { get; set; }

    }
}